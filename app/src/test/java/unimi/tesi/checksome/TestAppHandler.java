package unimi.tesi.checksome;


import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.mockito.Mockito;
import static org.junit.Assert.*;

import java.util.ArrayList;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestAppHandler {

    private static PackageManager pm;
    private static AppCompatActivity application;

    @BeforeClass
    public static void setUp() {
        application = Mockito.mock(AppCompatActivity.class);
        pm = Mockito.mock(PackageManager.class);
        Mockito.when(application.getPackageManager()).thenReturn(pm);
        Mockito.when(pm.getInstalledPackages(PackageManager.GET_META_DATA)).thenReturn(new ArrayList<PackageInfo>());

    }

    @Test
    public void test1Constructor(){
        assertTrue(new AppHandler(application)!=null);
        Mockito.verify(application, Mockito.times(1)).getPackageManager();
        Mockito.verify(pm, Mockito.times(1)).getInstalledPackages(PackageManager.GET_META_DATA);
    }

    @Test
    public void test2GenerateListApp(){
        assertTrue(new AppHandler(application).generateListAppName(true)!=null);
        Mockito.verify(application, Mockito.times(2)).getPackageManager();
        Mockito.verify(pm, Mockito.times(2)).getInstalledPackages(PackageManager.GET_META_DATA);

    }

    @Test
    public void test3IsSystemApp(){
        PackageInfo pack = Mockito.mock(PackageInfo.class);
        pack.applicationInfo = new ApplicationInfo();
        pack.applicationInfo.flags = 1;
        assertTrue(AppHandler.isSystemApp(pack));
    }

}
