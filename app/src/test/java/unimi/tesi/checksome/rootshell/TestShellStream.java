package unimi.tesi.checksome.rootshell;


import org.junit.Test;
import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class TestShellStream {

    @Test
    public void testRun() throws InterruptedException{
        InputStream in = new ByteArrayInputStream("test data".getBytes());
        List<String> list = new ArrayList<String>();
        ShellStream stream = new ShellStream(in, list);
        stream.start();
        stream.join();
        assertEquals("test data", list.get(0));
    }

    @Test
    public void testRunMoreElements() throws InterruptedException{
        InputStream in = new ByteArrayInputStream("first row\nsecond row\nthird row".getBytes());
        List<String> list = new ArrayList<String>();
        ShellStream stream = new ShellStream(in, list);
        stream.start();
        stream.join();
        assertEquals("first row", list.get(0));
        assertEquals("second row", list.get(1));
        assertEquals("third row", list.get(2));
    }

    @Test
    public void testRunNoElements() throws InterruptedException{
        InputStream in = new ByteArrayInputStream("".getBytes());
        List<String> list = new ArrayList<String>();
        ShellStream stream = new ShellStream(in, list);
        stream.start();
        stream.join();
        assertEquals(0, list.size());
    }

    @Test
    public void testRunNoElements2() throws InterruptedException{
        InputStream in = new ByteArrayInputStream("\n".getBytes());
        List<String> list = new ArrayList<String>();
        ShellStream stream = new ShellStream(in, list);
        stream.start();
        stream.join();
        assertEquals("", list.get(0));
        assertEquals(1, list.size());
    }
}
