package unimi.tesi.checksome;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import java.lang.reflect.Array;
import java.util.ArrayList;

import unimi.tesi.checksome.navigation.AppPagerAdapter;

public class CheckApp extends AppCompatActivity {

    private DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_app);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ViewPager view;
        PagerAdapter adapter;
        setSupportActionBar(toolbar);

        Intent data = getIntent();
        String packageName=data.getStringExtra("package_name");
        String appName = data.getStringExtra("app_name");
        ArrayList<String> listFiles = data.getStringArrayListExtra("list_files");
        toolbar.setSubtitle(appName);

        PackageManager pm = getPackageManager();
        PackageInfo currentPack = null, packageInfo = null;
        try {
            packageInfo = pm.getPackageInfo(packageName, PackageManager.GET_PERMISSIONS);
            currentPack = pm.getPackageInfo(getPackageName(), 0);
        }catch(PackageManager.NameNotFoundException e){
            toolbar.setSubtitle("App not found");
        }

        view = (ViewPager) findViewById(R.id.pager);
        adapter = new AppPagerAdapter(getSupportFragmentManager(), currentPack, packageInfo, listFiles);

        view.setAdapter(adapter);
        final TabLayout tabLayout = (TabLayout) findViewById(R.id.tablayout);
        tabLayout.setupWithViewPager(view);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            navigationView.setNavigationItemSelectedListener(
                    new NavigationView.OnNavigationItemSelectedListener() {
                        @Override
                        public boolean onNavigationItemSelected(MenuItem menu) {
                            switch (menu.getItemId()) {
                                case R.id.action_category_1:
                                    tabLayout.getTabAt(0).select();
                                    break;
                                case R.id.action_category_2:
                                    tabLayout.getTabAt(1).select();
                                    break;
                            }

                            drawer.closeDrawers();
                            return true;
                        }
                    });
        }


    }

}