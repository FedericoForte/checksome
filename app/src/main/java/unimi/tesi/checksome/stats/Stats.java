package unimi.tesi.checksome.stats;


import android.content.pm.PackageInfo;
import android.webkit.MimeTypeMap;

import java.util.ArrayList;
import java.util.List;

import unimi.tesi.checksome.rootshell.NoRootException;
import unimi.tesi.checksome.rootshell.Shell;

public class Stats {

    private List<String> listData;
    private long fileScanned;
    private long numTotFiles;

    private long numTextFiles = 0;
    private long numXmlFiles = 0;
    private long numImageFiles = 0;
    private long numOther = 0;

    public Stats(PackageInfo pack) throws NoRootException{
        fileScanned = 0;
        if(Shell.availableRoot()) {
            listData = Shell.run("du -a " + pack.applicationInfo.dataDir);
            if(listData.size()==0) {
                numTotFiles = 1;
                fileScanned=1;
            }else
                numTotFiles = listData.size();
            //du -a pattern --> diskUsage path/fileName --> example 4 ./data/data/file.txt
        }else
            throw new NoRootException("No root available!");

    }

    public List<String> getStats(){
        List<String> files = new ArrayList<String>();
        String[] row = null;
        for(String s: listData) {
            //split disk usage and use only filename
            row = s.split("\t");
            if (row[1] != null) {
                String extension = MimeTypeMap.getFileExtensionFromUrl(row[1]);
                String nameFile = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
                if(nameFile == null)
                    numOther++;
                else if(nameFile.startsWith("text/plain"))
                    numTextFiles++;
                else if(nameFile.startsWith("text/xml"))
                    numXmlFiles++;
                else if(nameFile.startsWith("image/"))
                    numImageFiles++;
                else
                    numOther++;

            }
            fileScanned++;

        }
        addStats(files);
        return files;
    }

    private void addStats(List<String> files){
        files.add("Number of files: " + fileScanned);
        files.add("Plain text files: " + numTextFiles);
        files.add("Xml files: " + numXmlFiles);
        files.add("Image files: " + numImageFiles);
        files.add("Other files: " + numOther);
    }


    public int getNumberFileScanned(){
        return (int)fileScanned;
    }

    public int getTotalFileScanned(){
        return (int)numTotFiles;
    }


}
