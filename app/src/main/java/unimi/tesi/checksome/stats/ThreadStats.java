package unimi.tesi.checksome.stats;

import java.util.List;

public class ThreadStats extends Thread{

    private List<String> res;
    private Stats handlerStats;

    public ThreadStats(List<String> listFiles, Stats f){
        this.res = listFiles;
        handlerStats = f;
    }

    @Override
    public void run() {
            res.addAll(handlerStats.getStats());
    }
}
