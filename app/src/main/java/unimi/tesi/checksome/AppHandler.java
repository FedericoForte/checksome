package unimi.tesi.checksome;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.List;

public class AppHandler {

    private AppCompatActivity application;
    private PackageManager pm;
    private List<PackageInfo> listInstalledApp;

    public AppHandler(AppCompatActivity application){
        this.application = application;
        pm = application.getPackageManager();
        listInstalledApp = pm.getInstalledPackages(PackageManager.GET_META_DATA);
    }

    //if noSystemApp==true ArrayAdapter doesn't contains system apps
    public AppInfo generateListAppName(boolean noSystemApp) {
        ArrayAdapter<String> listAppName = new ArrayAdapter<String>(application, android.R.layout.simple_list_item_1);
        ArrayList<String> listPackageName = new ArrayList<String>();
        for (PackageInfo pack : listInstalledApp) {
            if (!(noSystemApp && isSystemApp(pack))) {
                listAppName.add(pack.applicationInfo.loadLabel(pm).toString());
                listPackageName.add(pack.packageName);
            }
        }
        return new AppInfo(listAppName, listPackageName);
    }

    public static boolean isSystemApp(PackageInfo pack){
        return (pack.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0;
    }

    public static class AppInfo {

        public final ArrayList<String> listPackageName;
        public final ArrayAdapter<String> listAppName;

        public AppInfo(ArrayAdapter<String> adapter, ArrayList<String> list){
            listPackageName = list;
            listAppName = adapter;
        }
    }

}
