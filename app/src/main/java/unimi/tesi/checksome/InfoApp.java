package unimi.tesi.checksome;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

public class InfoApp extends AppCompatActivity implements View.OnClickListener {

    private String packageName = null;
    private String appName = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_app);
        packageName=getIntent().getStringExtra("package_name");
        infoApp(packageName);
        Button checkIt=(Button)findViewById(R.id.button);
        checkIt.setOnClickListener(this);
    }

    @Override
    public void onClick(View v){
        if(v.getId()==R.id.button){
            Intent intentApp = new Intent(getApplicationContext(), Progress.class);
            intentApp.putExtra("package_name", packageName);
            intentApp.putExtra("app_name", appName);
            startActivity(intentApp);
        }
    }

    private void infoApp(String packageName){
        //title
        TextView title = (TextView) findViewById(R.id.app_name);
        try {
            PackageManager pm=getPackageManager();
            PackageInfo app = pm.getPackageInfo(packageName, 0);
            appName = app.applicationInfo.loadLabel(pm).toString();
            title.setText(appName);
            //list info
            ListView listView = (ListView) findViewById(R.id.info_app);
            ArrayAdapter<String> listInfo= new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
            //app version
            listInfo.add("Version: " + app.versionName);
            //installation date
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss");
            String date= sdf.format(new Date(app.firstInstallTime));
            listInfo.add("Installation date: " + date );
            listInfo.add("Private directory: " + app.applicationInfo.dataDir );
            listInfo.add("APK directory: " + app.applicationInfo.sourceDir );
            listView.setAdapter(listInfo);
        }catch (PackageManager.NameNotFoundException e) {
            title.setText("App not found!" + packageName);
        }
    }
}
