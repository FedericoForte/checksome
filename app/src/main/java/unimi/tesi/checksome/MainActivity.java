package unimi.tesi.checksome;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;


public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{

    private AppHandler.AppInfo appInfo = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AppHandler handler = new AppHandler(this);
        ListView list = (ListView) findViewById(R.id.list_app);
        appInfo = handler.generateListAppName(true);
        list.setAdapter(appInfo.listAppName);
        list.setOnItemClickListener(this);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String packageName = appInfo.listPackageName.get(position);
        Intent intentApp = new Intent(getApplicationContext(), InfoApp.class);
        intentApp.putExtra("package_name", packageName);
        startActivity(intentApp);
    }
}
