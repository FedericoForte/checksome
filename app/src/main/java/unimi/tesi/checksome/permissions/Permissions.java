package unimi.tesi.checksome.permissions;


import android.content.pm.PackageInfo;

import java.util.ArrayList;

public class Permissions {

    public static ArrayList<String> getRequestedPermissions(PackageInfo pack){
        ArrayList<String> listPerm = new ArrayList<String>();
        if(pack.requestedPermissions!=null)
            for (String perm : pack.requestedPermissions)
                listPerm.add(perm);
        else
            listPerm.add("No requested permissions!");
        return listPerm;
    }
}
