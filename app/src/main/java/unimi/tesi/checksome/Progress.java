package unimi.tesi.checksome;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import unimi.tesi.checksome.stats.Stats;
import unimi.tesi.checksome.stats.ThreadStats;
import unimi.tesi.checksome.rootshell.NoRootException;

public class Progress extends AppCompatActivity {

    private ProgressDialog progressBar;
    private int progressBarStatus = 0;
    private Handler progressBarHandler = new Handler();

    private Stats fileHandler = null;
    private List<String> listFiles;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress);
        TextView subTitle = (TextView) findViewById(R.id.subTitle);
        Intent data = getIntent();
        final String packageName =data.getStringExtra("package_name");
        final String appName = data.getStringExtra("app_name");
        subTitle.setText(appName);
        PackageInfo packageInfo = null;
        PackageManager pm = getPackageManager();
        try {
            packageInfo = pm.getPackageInfo(packageName, PackageManager.GET_PERMISSIONS);
            fileHandler = new Stats(packageInfo);
        }catch(PackageManager.NameNotFoundException e){
        }catch(NoRootException e){}

        startProcessDialog(packageName,appName);
        if(fileHandler!=null)
          startAnalysis();


    }


    private void startProcessDialog(final String packageName,final String appName){
        // prepare for a progress bar dialog
        progressBar = new ProgressDialog(this);
        progressBar.setCancelable(false);
        progressBar.setMessage("Analysis in progress ...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressBar.setProgress(0);
        progressBar.setMax(100);
        progressBar.show();
        //reset progress bar status
        progressBarStatus = 0;

        new Thread(new Runnable() {
            public void run() {
                while (progressBarStatus < 100) {
                    if(fileHandler!=null)
                        progressBarStatus = calculateProgress();
                    else
                        progressBarStatus = 100;

                    // Update the progress bar
                    progressBarHandler.post(new Runnable() {
                        public void run() {
                            progressBar.setProgress(progressBarStatus);
                        }
                    });
                }

                //progressbar completed
                if (progressBarStatus >= 100) {
                    // sleep 1 seconds, so that you can see the 100%
                    try {
                        Thread.sleep(1000);

                    } catch (InterruptedException e) {}
                    // close the progress bar dialog
                    progressBar.dismiss();
                    Intent intentApp = new Intent(getApplicationContext(), CheckApp.class);
                    intentApp.putExtra("package_name", packageName);
                    intentApp.putExtra("app_name", appName);
                    if(fileHandler!=null) {
                        ArrayList<String> l = new ArrayList<String>();
                        l.addAll(listFiles);
                        intentApp.putStringArrayListExtra("list_files", l);
                    }
                    startActivity(intentApp);
                }
            }
        }).start();
    }


    private void startAnalysis(){

        listFiles = Collections.synchronizedList(new ArrayList<String>());
        ThreadStats t = new ThreadStats(listFiles, fileHandler);
        t.setPriority(Thread.MAX_PRIORITY);
        t.start();

    }


    //calculate ProgressBar
    private int calculateProgress() {
        float tot = fileHandler.getTotalFileScanned();
        float fileScanned = fileHandler.getNumberFileScanned();
        return Math.round(fileScanned/tot*100);
    }

}
