package unimi.tesi.checksome.rootshell;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Shell {

    private static final String ID = "id";
    private static final String ROOT ="uid=0"; //root uid=0

    public static List<String> run(String[] commands) {

        List<String> res = Collections.synchronizedList(new ArrayList<String>());

        try {
            // setup our process, retrieve input stream and output stream
            Process process = Runtime.getRuntime().exec("su");
            DataOutputStream inputStream = new DataOutputStream(process.getOutputStream());
            ShellStream outputCatcher = new ShellStream(process.getInputStream(), res);
            //outputCatcher begins to catch output by shell and write our commands to the shell
            outputCatcher.start();
            for (String write : commands) {
                inputStream.write((write + "\n").getBytes("UTF-8"));
                inputStream.flush();
            }
            inputStream.write("exit\n".getBytes("UTF-8"));
            inputStream.flush();

            // wait for our process to finish, while we catch output in background
            process.waitFor();
            //close our inputStream channel
            try {
                inputStream.close();
            } catch (IOException e) {
                //already closed, nothing to do
            }
            //make sure our thread(outputCatcher) catches all output by shell
            outputCatcher.join();
            //destroy our process(our shell), we don't need it anymore
            process.destroy();
            // in case of su, 255 usually indicates access denied
            if (process.exitValue() == 255) {
                res = null;
            }
        }catch(IOException e){
            return null;
        }catch(InterruptedException e){
            return null;
        }
        return res;
    }

    public static List<String> run(String command) {
        return Shell.run(new String[]{ command });
    }

    //check if a shell root is available
    public static boolean availableRoot() {
        List<String> ret = run(ID);
        return Shell.parseAvailableResult(ret);
    }

    //function used in function "availableRoot" that parses result
    private static boolean parseAvailableResult(List<String> ret) {
        if (ret == null)
            return false;

        for (String line : ret) {
            if (line.contains(ROOT))
                return true;

        }
        return false;
    }
}