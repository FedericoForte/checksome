package unimi.tesi.checksome.rootshell;


public class NoRootException extends Exception{

    public NoRootException(String message) {
        super(message);
    }

}
