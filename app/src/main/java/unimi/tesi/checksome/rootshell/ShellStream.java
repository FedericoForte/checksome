package unimi.tesi.checksome.rootshell;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

//read and write shell
public class ShellStream extends Thread {

    private BufferedReader reader = null;
    private List<String> writer = null;

    public ShellStream(InputStream inputStream, List<String> outputList) {
        reader = new BufferedReader(new InputStreamReader(inputStream));
        writer = outputList;
    }

    @Override
    public void run() {

        try {
            String line;
            while ((line = reader.readLine()) != null)
                if (writer != null)
                    writer.add(line);
            reader.close();
        } catch (IOException e) {
            // reader already closed -- nothing to do
        }
    }
}
