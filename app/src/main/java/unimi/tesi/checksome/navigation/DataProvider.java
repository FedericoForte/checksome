package unimi.tesi.checksome.navigation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataProvider {
    public static final String CATEGORY="CATEGORY";
    public static final String PERMISSION_CATEGORY ="PERMISSIONS";
    public static final String STATS_CATEGORY ="STATS";

    private static final String[] CATEGORY_ORDER=new String[]{PERMISSION_CATEGORY, STATS_CATEGORY};

    public static class Content {
        private String data;

        public Content(String data){
            this.data = data;
        }

        @Override
        public String toString() {
            return data;
        }


    }

    private static HashMap<String,ArrayList<Content>> contents;

    static {
        contents=new HashMap<String,ArrayList<Content>>();
        ArrayList<Content> articles=new ArrayList<Content>();
        contents.put(STATS_CATEGORY, articles);

        articles=new ArrayList<Content>();
        contents.put(PERMISSION_CATEGORY, articles);
    }

    public static List<Content> getContentsByCategory(String category) {
        return contents.get(category);
    }

    public static String getCategoryByIndex(int index)
    {
        return CATEGORY_ORDER[index];
    }

    public static int getCategoriesCount()
    {
        return contents.entrySet().size();
    }

}