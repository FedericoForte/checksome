package unimi.tesi.checksome.navigation;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

public class ContentFragment extends ListFragment {
    private ArrayAdapter<DataProvider.Content> adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        Bundle b=getArguments();
        String category= b.getString(DataProvider.CATEGORY);
        ArrayList<String> list = b.getStringArrayList("list");
        adapter = new ArrayAdapter<DataProvider.Content>(inflater.getContext(), android.R.layout.simple_list_item_1);
        for(String s: list)
            adapter.add(new DataProvider.Content(s));
        setListAdapter(adapter);
        return super.onCreateView(inflater, container, savedInstanceState);
    }
}
