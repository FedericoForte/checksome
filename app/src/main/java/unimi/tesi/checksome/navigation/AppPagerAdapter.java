package unimi.tesi.checksome.navigation;

import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

import unimi.tesi.checksome.permissions.Permissions;

public class AppPagerAdapter extends FragmentPagerAdapter {

    private PackageInfo pack, currentPack;
    private ArrayList<String> listFiles;

    public AppPagerAdapter(FragmentManager manager, PackageInfo currentPack, PackageInfo pack, ArrayList<String> listFiles) {
        super(manager);
        this.pack = pack;
        this.currentPack = currentPack;
        this.listFiles = listFiles;
    }

    @Override
    public Fragment getItem(int index) {
        Bundle b=new Bundle();
        ContentFragment fragment=null;
        String category=DataProvider.getCategoryByIndex(index);
        //put category in bundle to ContentFragment
        b.putString(DataProvider.CATEGORY,category);
        if(category.equals(DataProvider.PERMISSION_CATEGORY))
            b.putStringArrayList("list", Permissions.getRequestedPermissions(pack));
        else
            if(listFiles ==null || listFiles.size()==0) {
                ArrayList<String> l = new ArrayList<String>();
                l.add("No files");
                b.putStringArrayList("list", l);
            }else
                b.putStringArrayList("list", listFiles);
        fragment=new ContentFragment();
        fragment.setArguments(b);

        return fragment;
    }

    @Override
    public int getCount()
    {
        return DataProvider.getCategoriesCount();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return DataProvider.getCategoryByIndex(position);
    }
}